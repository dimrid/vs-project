﻿using System;

namespace theFirstPractice
{
    public class Program
    {
        static void Main()
        {
            Console.Write("Введите размер массива(нужно ввести число): ");
            int userInput = int.Parse(Console.ReadLine());

            var array = new int[userInput];
            Random rnd = new Random();
            double sumResult = 0.0;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = rnd.Next(1, userInput);

                Console.WriteLine($"Числа в массиве: {array[i]}.");
            }

            for (int i = 0; i < array.Length; i++)
            {

                sumResult += array[i]; // считаем сумму

            }
            sumResult /= array.Length; 
            Console.WriteLine($"\nСреднее арифметическое элементов массива равно: {Math.Round(sumResult, 3)}");
        }
    }
}
